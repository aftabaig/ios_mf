//
//  MFAppDelegate.h
//  MerchantFramework
//
//  Created by Aftab Baig on 22/08/2014.
//  Copyright (c) 2014 AftaBaig. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
