//
//  main.m
//  MerchantFramework
//
//  Created by Aftab Baig on 22/08/2014.
//  Copyright (c) 2014 AftaBaig. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MFAppDelegate class]));
    }
}
